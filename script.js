/*Асинхронність в JavaScript означає, що певні операції можуть виконуватись паралельно, не чекаючи завершення попередніх операцій.
 Замість цього, код може продовжувати виконуватись далі, поки асинхронна операція виконується у фоновому режимі.
Наприклад, відправлення запитів на сервер,завантаження ресурсів з сервера.*/


class ipData{
    constructor(continent,country,regionName,city,district) {
        this.continent = continent;
        this.country = country;
        this.regionName = regionName;
        this.city = city;
        this.district = district;
        for (let key in this){
            if(this[key] === ''){
                this[key] = 'Unknown'
            }
        }
    }
}


async function getIp(){
    try {
        let response = await fetch('https://api.ipify.org/?format=json');
        return await response.json();

    }catch (err){
        console.log(err);
    }

}

async function showData(){
    let ip = await getIp();
    let ipData = await getIpData(ip);
    showDataOnPage(ipData);
}

function showDataOnPage(data){
    let dataInfo = `Continent : ${data.continent} <br> Country : ${data.country} <br> Region Name : ${data.regionName} <br>
     City : ${data.city} <br> District : ${data.district}`;
    document.getElementById('placeholder').innerHTML = dataInfo;
}

async function getIpData(ip){
    let response = await fetch('http://ip-api.com/json/' + ip.ip + '?fields=continent,country,regionName,city,district');
    let json = await response.json();
    let dataObj = new ipData(json.continent, json.country,json.regionName,json.city, json.district);
    return dataObj;
}

document.getElementById('btn-ip').addEventListener('click', showData);